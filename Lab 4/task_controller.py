''' @file           task_controller.py
    @brief A task used to implement a P controller to run a motor and encoder
    @details This task when called creates a class object that takes in 8 shares 
             to set and execute the methods in it. From there it writes the value
             of the velocity and encoder value and shares it to the task user where it is
             compiled and printed.
             image here
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/18/21
    @page page Controller Testing
          Controller was tested using various gains and set speeds.
          Below are various trials conducted at different gains.
          It was found that as the gains increased the motor varied less in speed
          \image html stepresponse1.png "Step Response Using -10 Gain at Target 56 Rad/s"
          \image html stepresponse2.png "Step Response Using -8 Gain at Target 56 Rad/s"
          \image html stepresponse3.png "Step Response Using -15 Gain at Target 56 Rad/s"
          \image html stepresponse4.png "Step Response Using -20 Gain at Target 56 Rad/s"
          
          The controller tested above can be visualized in the block diagram below
          \image html block_diagram.jpg "Controller Block Diagram"
          
          To run the controller various task are running simultaniously.
          Below is an image of the Task Digram used to run the controller
          \image html Task_Diagram.jpg "Task Diagram"
          
          Lastly, the images below are all the state transition diagram for each task
          that implemented a Finite State Machine.
          \image html encoder_fsm.jpg "Task_Encoder Finite State Machine Diagram"
          \image html user_fsm.jpg "Task_Uer Finite State Machine Diagram"
          \image html controller_fsm.jpg "Task_Controller Finite State Machine Diagram"
          \image html motor_fsm.jpg "Task_Motor Finite State Machine Diagram"
          

    
'''
import Driver_Controller
import utime

class task_controller():
     ''' @brief     A task for keyboard inputs to call for encoder data
         @details   This class is a task taking in a user input through 
                    a single keyboard key input and displaying the related 
                    information to the input 
     '''
     def __init__(self, vel_share, encoder1_share, encoder2_share, gain_share, state_control_share, motor_control_share, motor1_share, motor2_share):
        ''' @brief
            @details
        '''    
        ## Initialisation of error variable
        self.error = 0
        
        ## Initializes a share that shares the velocity variable
        self.vel_share = vel_share
        
        ## Initializes a share that shares the encoder variable
        self.encoder1_share = encoder1_share
        
        ## Initializes a share that shares the encoder variable
        self.encoder2_share = encoder2_share

        ## Initializes a share that shares the gain variable
        self.gain_share = gain_share
        
        ## Initializes a share that sets the state in which the controller driver needs to be in
        self.state_control_share = state_control_share
        
        ## Initializes a share that sets the motor to be used
        self.motor_control_share = motor_control_share
        
        ## Initializes a share that shares the data for the motor
        self.motor1_share = motor1_share
        
        ## Initializes a share that shares the data for the motor
        self.motor2_share = motor2_share
    
        ## Initializes a controller driver class object to be used later in the file
        self.controller = Driver_Controller.Driver_Controller()
        
        ## sets the data collection period in which the file will run
        self.period = 5
         
        ## sets when the next time data will be run
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
     def run(self):
         ''' @brief Runs one iteration of the FSM every time it is called
            @details This method runs the encoder FSM machine 1 time and returns
                     the position that the encoder reads of the shaft
         '''
         ## Creates a current time that updates everytime the code is ran. This current
         # time sets the rate at which this program will run
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state_control_share.read() == 1:
                self.controller.set_Kp(self.gain_share.read())
                self.state_control_share.write(2)
            elif self.state_control_share.read() == 2:
                if self.motor_control_share.read() == 1:
                    self.motor1_share.write(self.controller.run(self.vel_share.read(), self.encoder1_share[1].read()))
                    print(self.encoder1_share[1].read())
                if self.motor_control_share.read() == 2:
                    self.motor2_share.write(self.controller.run(self.vel_share.read(), self.encoder2_share[1].read()))
            else: 
                pass
            self.next_time = utime.ticks_add(self.next_time, self.period)
            