''' @file           task_user.py
    @brief A task used to check for and implement specific keyboard commands
    @details This task when called will create a task user object from the task
             user class. this class contains 3 methods to perform different tasks.
             The first method initalizes the task suer object. The next method
             when called prompts the user with the specific commands they can use
             and then transition into waiting for a command before running any code.
             From there dependsing on the key stroke it sends data back to the main
             or sends the user to the third method. In the third method the program
             runs through collecting data for 30 seconds or until a stop command is
             issued. From there is prints the data in a list and then runs back to
             the second method.
             \image html IMG_8178.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
    @copyright license term
'''
import pyb, utime, array
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)

## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)

## State 2 of the user interface task
S2_COLLECT_DATA     = const(2)

## State 3 of the user interface task
S3_COLLECT_DATA     = const(3)

## State 4 of the user interface task
S4_COLLECT_INPUT     = const(4)

## State 5 of the user interface task
S5_COLLECT_INPUT     = const(5)

## State 6 of the user interface task
S6_STEP_RESPONSE     = const(6)

## State 7 of the user interface task
S7_STEP_RESPONSE     = const(7)

## State 8 of the user interface task
S_print = const(8)

## State 9 of the user interface task
State_print_num = const(9)

## State 10 of the user interface task
S10_COLLECT_INPUT     = const(10)

## State 11 of the user interface task
S11_COLLECT_INPUT     = const(11)

class task_user():
     ''' @brief     A task for keyboard inputs to call for encoder data
         @details   This class is a task taking in a user input through 
                    a single keyboard key input and displaying the related 
                    information to the input 
     '''
     def __init__(self, motor1_share, encoder1_share, motor2_share, encoder2_share, zflag1, zflag2, fault_share, vel_share, gain_share, state_control_share, motor_control_share):
         ''' @brief         Initializes task_user
             @details       This initiates an User objects and prepare 
                            keyboard serial input and other variables
             @param motor1_share inputs a share that links the data from this file
                   to the user task so they can communicate
             @param motor2_share inputs a share that links the data from this file
                   to the user task so they can communicate
             @param encoder1_share inputs a share that links the data from this file
                   to the user task so they can communicate
             @param encoder2_share inputs a share that links the data from this file
                   to the user task so they can communicate
             @param zflag1 inputs a share that sahres a flag to zero the encoder between
                   the user task and here
             @param zflag1 inputs a share that shares a flag to zero the encoder between
                   the user task and here
             @param fault_share inputs a share that links the data from this file
                   to the user task so they can communicate and clear any faults that
                   occur
             @param vel_share inputs a share that shares the velocity setpoint from the task user
                    to the task controller
             @param gain_share inputs a share that shares the inputed gain to the task controller
             @param state_control_share inputs a share that when read will share the state that the
                    controller needs to be in
             @param motor_control_share inputs a share that specifies which motor will be run in the
                    controller
         '''
         ## A serial port to use for user I/O
         self.ser = pyb.USB_VCP()
        
         ## The state to run on the next iteration of the finite state machine
         self.state = S0_INIT
         
         # creates a list that will contain data for 30 seconds
#         self.time_array = array.array('i', range(3001))
#         
#         self.pos_array = array.array('d', range(3001))
#         
#         self.speed_array = array.array('d', range(3001))
#         
#         self.diff_array = array.array('d', range(3001))
         
         ## Creates an array of 1002 double type values
         self.vel_array = array.array('d', range(1001))
         
         ## Creates an array of 1002 integer type values
         self.vel_time_array = array.array('i', range(1001))
         
         ## Creates an array of 1002 double type values
         self.vel_diff_array = array.array('d', range(1001))
         
         ## Creates a variable for the inital duty cycle for the motors
         self.duty = '0'
         
         ## sets the data collection period or frequency
         self.period = 10
         
         ## sets when the next time data will be collected
         self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
         
         ## Initializes a share that shares an encoder variable
         self.encoder1_share = encoder1_share
         
         ## Initializes a share that shares a motor variable
         self.motor1_share = motor1_share
         
         ## Initializes a share that shares an encoder variable
         self.encoder2_share = encoder2_share
         
         ## Initializes a share that shares a motor variable
         self.motor2_share = motor2_share
         
         ## Initializes a share that shares the zero flag variable
         self.zflag1 = zflag1
         ## Initializes a share that shares the zero flag variable
         self.zflag2 = zflag2
         ## Initializes a share that shares the fault trigger variable
         self.fault_share = fault_share
         
         ## sets a count for the rate at which the file will print
         self.print_count = 0
         
         ## Initializes a share that shares the motor to be used
         self.motor_control_share = motor_control_share
         
         ## Initializes a share that shares the gain variable
         self.gain_share = gain_share
         
         ## Initializes a share that shares the velocity variable
         self.vel_share = vel_share
         
         ## Initializes a share that sets the state of the controller
         self.state_control_share = state_control_share
         
         
     def intro(self):
         ''' @brief     Introduce and run the user task.
             @details   Uses a finite state machine to interact with the user
                        inputs.Characters are entered through a serial
                        port that cmmunicating through a USB keyboard.
             @param     enc_pos     Updated data collection from task encoder.
             @return    Flag indicating if the encoder needs to be zeroed.
         '''
         ## Creates a timer at the current time to determine when the file runs
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
             if self.state == S0_INIT:
                 print("Welcome to the Nucleo Hardware!", 
                       "Type z to zero the position of encoder 1.",
                       "Type Z to zero the position of encoder 2.",
                       "Type p for the position of encoder 1.",
                       "Type P for the position of encoder 2.",
                       "Type d for the delta for encoder 1.",
                       "Type D for the delta for encoder 2.",
                       "Type m to set the duty cycle for motor 1.",
                       "Type M to set the duty cycle for motor 2.",
                       "Type c or C to clear any faults found.",
#                       "Type g to collect encoder 1 data for 30 sec.",
#                       "Type G to collect encoder 2 data for 30 sec.",
                       "Type s or S to end collection prematurely:",
                       "Type 1 to setup controller for motor 1",
                       "Type 2 to setup controller for motor 2", sep="\n")
                 
                 self.transition_to(S1_WAIT_FOR_CHAR)
             elif self.state == S1_WAIT_FOR_CHAR:
                if self.ser.any():
               
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                    char_in = self.ser.read(1).decode()
                
                    if (char_in == 'z'):
                        print('Encoder 1 Position Zeroed')
                        self.zflag1.write(True)
                    
                    elif (char_in == 'Z'):
                        print('Encoder 2 Position Zeroed')
                        self.zflag2.write(True)
                    
                    elif (char_in == 'p'):
                        print(self.encoder1_share[0].read())
                    
                    elif (char_in == 'P'):
                        print(self.encoder2_share[0].read())
                    
                    elif (char_in == 'd'):
                        print(self.encoder1_share[1].read())
                    
                    elif (char_in == 'D'):
                        print(self.encoder2_share[1].read())
                    
                    elif (char_in == 'm'):
                        print('Enter Motor Duty for Motor 1')
                        self.duty = ''
                        self.transition_to(S4_COLLECT_INPUT)
                    
                    elif (char_in == 'M'):
                        print('Enter Motor Duty for Motor 2')
                        self.duty = ''
                        self.transition_to(S5_COLLECT_INPUT)
                    
                    elif (char_in == 'c' or char_in == 'C'):
                        print('Clear Fault')
                        self.fault_share.write(True)
                    
#                    elif (char_in == 'g'):
#                        print ('Collecting Data from Encoder 1')
#                        ## counts the number of times data is collected
#                        self.runs = 0
#                        self.transition_to(S2_COLLECT_DATA)
#                        
#                    elif (char_in == 'G'):
#                        print ('Collecting Data from Encoder 2')
#                        self.runs = 0
#                        self.transition_to(S3_COLLECT_DATA)
#                        
                    elif (char_in == '1'):
                        print ('Collecting Data from Encoder 1')
                        self.motor_control_share.write(1)
                        print('Please enter the gain for P:')
                        self.transition_to(S10_COLLECT_INPUT)
                        
                    elif (char_in == '2'):
                        print ('Collecting Data from Encoder 2')
                        self.motor_control_share.write(2)
                        print('Please enter the gain for P:')
                        self.transition_to(S10_COLLECT_INPUT)
                        
                
#             elif self.state == S2_COLLECT_DATA:                     
#                 if self.runs < (30e4/self.period)+1:
#                     
#                     ## records position data from the encoder
#                    self.pos_array[self.runs] = self.encoder1_share[0].read()
#                    self.speed_array[self.runs] = self.encoder1_share[1].read()
#                    self.time_array[self.runs] = utime.ticks_ms()
#                    self.runs += 1
#                     
#                 if self.ser.any():
#                    char_in = self.ser.read(1).decode()
#                    if (char_in == 's' or char_in == 'S'):
#                        print('Data Collection Ended Early')
#                        self.transition_to(S_print)
#                 elif self.runs >= (30e4/self.period)+1:
#                    self.transition_to(S_print)
#                 else: 
#                    pass
#
#             elif self.state == S3_COLLECT_DATA:
#                 
#                 if self.runs < (30e4/self.period)+1:
#                     
#                    ## records position data from the encoder
#                    self.pos_array[self.runs] = self.encoder2_share[0].read()
#                    self.speed_array[self.runs] = self.encoder2_share[1].read()
#                    self.time_array[self.runs] = utime.ticks_ms()
#                    self.runs += 1
#                     
#                 if self.ser.any():
#                     char_in = self.ser.read(1).decode()
#                     if (char_in == 's' or char_in == 'S'):
#                        print('Data Collection Ended Early')
#                        self.transition_to(S_print)
#                 elif self.runs >= (30e4/self.period)+1:
#                    self.transition_to(S_print)
#                 else: 
#                     pass
#                    
#             elif self.state == S_print:
#                 if self.print_count < self.runs:                              
#                    self.diff_array[self.print_count] = utime.ticks_diff(self.time_array[self.print_count], self.time_array[0])/1000
#                    print(self.diff_array[self.print_count], self.pos_array[self.print_count], self.speed_array[self.print_count])
#                    self.print_count += 1  
#                 else:
#                    self.time_array = array.array('i', range(3001))
#                    self.pos_array = array.array('d', range(3001))
#                    self.speed_array = array.array('d', range(3001))
#                    self.diff_array = array.array('d', range(3001))
#                    self.print_count = 0
#                    self.transition_to(S1_WAIT_FOR_CHAR)

                            
             elif self.state == S4_COLLECT_INPUT:
                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '.':
                        self.duty += '.'
                        self.ser.write(char_in)
                    elif char_in == '\x7F':
                        if len(self.duty) == 0:
                            pass
                        else:
                            self.duty = self.duty[:-1]
                            self.ser.write(char_in)
                    elif char_in == '\r' or '\n':
                        if len(self.duty) == 0:
                            print("Please Enter a value")
                        else:
                            self.motor1_share.write(float(self.duty))
                            print("")
                            self.transition_to(S1_WAIT_FOR_CHAR)
                
             elif self.state == S5_COLLECT_INPUT:
                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '.':
                        self.duty += '.'
                        self.ser.write(char_in)
                    elif char_in == '\x7F':
                        if len(self.duty) == 0:
                            pass
                        else:
                            self.duty = self.duty[:-1]
                            self.ser.write(char_in)
                    elif char_in == '\r' or '\n':
                        if len(self.duty) == 0:
                            print("Please Enter a value")
                        else:
                            self.motor2_share.write(float(self.duty))
                            print("")
                            self.transition_to(S1_WAIT_FOR_CHAR)
            
             elif self.state == S6_STEP_RESPONSE:           
                 if self.runs < (10e3/self.period)+1:
                         
                    ## records position data from the encoder
                    self.vel_array[self.runs] = self.encoder1_share[1].read()
                    ## records time data for the run
                    self.vel_time_array[self.runs] = utime.ticks_ms()
                    self.runs += 1
                     
                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if (char_in == 's' or char_in == 'S'):
                        print('Data Collection Ended Early')
                        self.state_control_share.write(0)
                        self.transition_to(State_print_num)
                 elif self.runs >= (10e3/self.period)+1:
                     self.state_control_share.write(0)
                     self.transition_to(State_print_num)
                 elif self.fault_share.read() == True:
                     print('Data Collection Ended Early')
                     self.state_control_share.write(0)
                     self.transition_to(State_print_num)
                 else: 
                     pass
    
                 
             elif self.state == S7_STEP_RESPONSE:
                 
                 if self.runs < (10e3/self.period)+1:
                         
                    self.vel_array[self.runs] = self.encoder2_share[1].read()
                    self.vel_time_array[self.runs] = utime.ticks_ms()
                    self.runs += 1

                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if (char_in == 's' or char_in == 'S'):
                        print('Data Collection Ended Early')
                        self.state_control_share.write(0)
                        self.transition_to(State_print_num)
                 elif self.runs >= (10e3/self.period)+1:
                     self.state_control_share.write(0)
                     self.transition_to(State_print_num)
                 elif self.fault_share.read() == True:
                     print('Data Collection Ended Early')
                     self.state_control_share.write(0)
                     self.transition_to(State_print_num)
                 else: 
                     pass
                    
             elif self.state == State_print_num:
                 if self.print_count < self.runs:    
                     ## Creates an array to report the difference in the recorded time and the start time                        
                     self.vel_diff_array[self.print_count] = utime.ticks_diff(self.vel_time_array[self.print_count], self.vel_time_array[0])/1000
                     print(self.vel_array[self.print_count]) #self.vel_diff_array[self.print_count], 
                     self.print_count += 1  
                 else:
                     self.vel_time_array = array.array('i', range(1001))
                     self.vel_array = array.array('d', range(1001))
                     self.vel_diff_array = array.array('d', range(1001))
                     self.print_count = 0
                     self.transition_to(S1_WAIT_FOR_CHAR)
             
             elif self.state == S10_COLLECT_INPUT:
                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '.':
                        self.duty += '.'
                        self.ser.write(char_in)
                    elif char_in == '\x7F':
                        if len(self.duty) == 0:
                            pass
                        else:
                            self.duty = self.duty[:-1]
                            self.ser.write(char_in)
                    elif char_in == '\r' or '\n':
                        if len(self.duty) == 0:
                            print("Please Enter a value")
                        else:
                            self.gain_share.write(float(self.duty))
                            print("")
                            print("Please enter the Velocity Setpoint:")
                            self.transition_to(S11_COLLECT_INPUT)
             elif self.state == S11_COLLECT_INPUT:
                 if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '.':
                        self.duty += '.'
                        self.ser.write(char_in)
                    elif char_in == '\x7F':
                        if len(self.duty) == 0:
                            pass
                        else:
                            self.duty = self.duty[:-1]
                            self.ser.write(char_in)
                    elif char_in == '\r' or '\n':
                        if len(self.duty) == 0:
                            print("Please Enter a value")
                        else:
                            self.vel_share.write(float(self.duty))
                            self.runs = 0
                            print("")
                            self.state_control_share.write(2)
                            if self.motor_control_share.read() == 1:
                                self.transition_to(S6_STEP_RESPONSE)
                            elif self.motor_control_share.read() == 2:
                                self.transition_to(S7_STEP_RESPONSE)
             self.next_time = utime.ticks_add(self.next_time, self.period)
                
                
     def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        self.state = new_state