'''@file                mainpage.py
   @brief               Main page for documentation site.
   @details             This is the main page for the doxygen site. It contains links to 
                        the code for each lab and some information about each one. 

   @mainpage

   @section sec_rep     Repository
                        The repository for all the Labs listed below and their original file version for each lab
                        can be found at https://bitbucket.org/chrisuzuki62/me305_labs

   @section sec_lab0    Lab 0: Fibonacci Sequence
                        Please see Lab_0.py for details.

   @section sec_lab1    Lab 1: LED
                        The first Nucleo lab, uses a Finite State Machine
                        to implement 3 LED blinking patterns which can be cycled through
                        by pressing a button. Patterns are square, sine, and
                        sawtooth waves.
                        Please see Lab_1.py for details and a demo.

   @section sec_lab2    Lab 2: Encoder Driver and Data Display
                        In this lab we created classes for an encoder driver, and tasks for updating the encoder and taking user input. 
                        These are used to create a program which lets the user
			print current readings for position and speed, as well as collect them over a 30 second period of time.
                        Please see the encoder.py, task_encoder.py, task_user.py files.

   @section sec_lab3	Lab 3: Motor Task and Driver
                			In this lab, we created classes for a motor driver and its corresponding task, then we updated 			
                        the user task to interact with the motor. This will allow for the user to set the duty cycle, 			
                        enable, and disable on both motors. 
                        Please see the DRV8847.py, task_motor.py, and updated task_user.py files.
                        
   @section sec_lab4	Lab 4: Closed Loop Speed Control
                			In this lab, we created classes for a controller driver and its corresponding task, then we updated 			
                        the user task to interact with the controller. This will allow for the user to set the P gain value. 
                        Please see the task_controller.py, Driver_Controller.py, and updated task_user.py files.
                        
   @section sec_lab5	Lab 5: I2C Inertial Measurement Units
                			In this lab, we created classes for a Inertial Sensor 
                        Please see the IMU.py
   
    @section sec_lab6 Lab 6:
                        In this lab, we linked lab 3, 4, 5 and 5 together with three new tasks to create a system that when ran
                        can balance a ball on a platform through closed loop control. In order to control the platform we use
                        the motor controller task and driver, the IMU driver, the closed loop controller task and driver, and
                        add in a task for the IMU, a collection task to retireve and print data out and a panel task and driver 
                        to read the position of ball on a resistive touch panel and then balnce it to the center of the platform.
                        Please see the IMU.py, DRV8847.py, task_motor.py, task_controller.py, Driver_Controller.py, task_collect.py,
                        task_IMU.py, task_panel.py, Panel_Driver.py, and task_user_tp.py files.
                        
   @author              Cade Liberty
   @author              Chris Suzuki

   @date                November 17, 2021
   @page    page1        Term Project Theory
                        The Term Project's Dyanmic System required simulation to understands its motion and required controller.
                        To create the equation of motion for the mechanism the below images shows the hand calculation derivation
                        for the overall systems motion.
                        \image html IMG_8314.jpg "Hand Calc Page 1"
                        \image html IMG_8315.jpg "Hand Calc Page 2"
                        \image html IMG_8316.jpg "Hand Calc Page 3"
                        
                        These equations were then re-expressed using MATLAB Symbolic Toolkit to prepare for the Simulation.
                        The symbolic derivation can be found by clicking here:<A HREF="Term_theory.html">Symbolic Derivation</A>
                       
                        The model used to simulate the open loop and closed loop simulation can be found below
                        \image html term_model1.JPG "Open Loop Model"
                        \image html term_model.JPG "Closed Loop Model"                       
                        
                        The results of the simulation can be found clicking the different scenarios below
                        
                        <A HREF="Term(Closed).html">Closed Loop Model with the ball at a 5cm offset from the center of the plate</A>
                        
                        <A HREF="Term(Open1).html">Open Loop Model with the ball at a 5cm offset from the center of the plate</A>
                        
                        <A HREF="Term(Open2).html">Open Loop Model with the ball at the center of the plate</A>
                        
   @page   page2        Term Project Report
                        Overall the term project requires many different files to come together and work cooperatively to balance the
                        ball on the platform. In order to organize our thinking, we first created a task diagram that links all of the
                        shared data between the different tasks that we have. The task diagram can be found below.
                        
                        task diagram here
                        
                        After making the task diagram we then created the state diagrams that are used to run each task. Below are the
                        finite state machines for the task_user_tp, task_collect, task_motor, task_IMU, task_panel, and task_controller
                        in the specified order.
                        
                        task_user_tp FSM here
                        
                        task_collect FSM here 
                        
                        task_motor FSM here 
                        
                        task_IMU FSM here 
                        
                        task_panel FSM here 
                        
                        task_controller FSM here
'''






