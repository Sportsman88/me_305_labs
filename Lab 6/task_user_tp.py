''' @file           task_user.py
    @brief A task used to check for and implement specific keyboard commands
    @details This task when called will create a task user object from the task
             user class. This class contains 3 methods to perform different tasks.
             The first method initalizes the task user object. The next method
             when called prompts the user with the specific commands they can use
             and then waits for a command to be inputted before running any code.
             The final method is used to transition between the two states in the
             second method.
             \image html 
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/8/21
    @copyright license term
'''
import pyb, utime
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)

## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)

class task_user():
     ''' @brief     A task for keyboard inputs used to operate the system
         @details   This class takes in the keystrokes from a keyboard and depending on
                    the stroke executes a specific command. The two commands that can be
                    executed are 1 which balances the board and g which collects data from
                    the board
     '''
     def __init__(self, collect_share, state_control_share):
         ''' @brief         Initializes task_user
             @details       This initiates the task user objects and prepare 
                            keyboard serial input and other variables
             @param collect_share  inputs a share that specifies when to collect data from the IMU and the panel
             @param state_collect_share inputs a share that states the state that the task controller needs to be in
         '''
         ## A serial port to use for user I/O
         self.ser = pyb.USB_VCP()
        
         ## The state to run on the next iteration of the finite state machine
         self.state = S0_INIT
         
         ## Creates a variable for the inital duty cycle for the motors
         self.duty = '0'
         
         ## sets the data collection period
         self.period = 10
         
         ## sets when the next time data will be collected
         self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
         
         ## Initializes a share that shares a variable to state when to collect data
         self.collect_share = collect_share
         
         ## Initializes a share that shares a variable that states what state the controller should be in
         self.state_control_share = state_control_share
         
     def intro(self):
         ''' @brief     Introduces and runs the user task.
             @details   Uses a 2 state finite state machine to interact with the user and call for inputs
                        that are used to control the system.Characters are entered through a serial
                        port that ccommunicates  through a USB keyboard.
         '''
         ## Creates a timer at the current time to determine when the file runs
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
             if self.state == S0_INIT:
                 print("Welcome to the Nucleo Hardware!", 
                       "Type 1 to begin balacning the board",
                       "Type g or G to start collecting data:",
                       "Type s or S to end collection prematurely:", sep="\n")
                 self.transition_to(S1_WAIT_FOR_CHAR)
                 
             elif self.state == S1_WAIT_FOR_CHAR:
                if self.ser.any():
               
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                    char_in = self.ser.read(1).decode()
                        
                    if (char_in == 'g' or char_in == 'G'):
                        print('Collecting data')
                        self.collect_share.write(True)
                    
                    elif (char_in == '1'):
                        print ("Board Balancing Starting")
                        self.state_control_share.write(1)
             self.next_time = utime.ticks_add(self.next_time, self.period)
        
     def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        self.state = new_state