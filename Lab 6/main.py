''' @file           main.py
    @brief Runs the main program and creates all objects to run and balance the panel and ball
    @details This program starts up on a reboot and runs through. It first creates
             all of the class objects from the task user, task motor, task collect,
             task IMU, task controller, and task panel classes and then uses them in 
             a loop fashion to retrieve data and share data between them both which
             is then used to balance the connected panel and board. Finally to exit 
             the user must enter a keyboard interupt.
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/8/21
    @copyright license term
'''
import task_IMU, shares, task_user_tp, task_motor, task_controller, task_panel, task_collect
    
#Running the code
if __name__ =='__main__':
    
    ## Creates two share objects that are used to set a motors to run
    motor_share = (shares.Share(0), shares.Share(0))
    ## Creates a share object that is used to set the state of the controller
    state_control_share = shares.Share(0)
    ## Initializes two shares that shares the the ADC reading from task panel
    ADC_share = (shares.Share(0), shares.Share(0))
    ## Creates 2 share object that are used to share the velocity of the ball as it moves
    ADC_vel_share = (shares.Share(0), shares.Share(0))
    ## Initializes 2 shares that share the euler angle reading from task IMU for roll and pitch
    euler_angle_share = (shares.Share(0), shares.Share(0))
    ## Initializes a share that signals when to collect data in task collect
    collect_share = shares.Share(False)
    ## Initializes 2 shares that share the angular veloicty reading from task IMU
    ang_vel_share = (shares.Share(0), shares.Share(0))
    
    ## Initalizes an object that is able to use the methods defined in the task user class
    task1 = task_user_tp.task_user(collect_share, state_control_share)
    ## Initalizes an object that is able to use the methods defined in the task motor class
    task2 = task_motor.task_motor(motor_share)
    ## Initalizes an object that is able to use the methods defined in the task controller class
    task3 = task_controller.task_controller(state_control_share, ADC_share, ADC_vel_share, euler_angle_share, ang_vel_share, motor_share)
    ## Initalizes a an object that is able to use the methods defined in the task IMU class
    task4 = task_IMU.task_IMU(euler_angle_share, ang_vel_share)
    ## Initalizes a an object that is able to use the methods defined in the task Panel class
    task5 = task_panel.task_panel(ADC_share, ADC_vel_share)
    ## Initalizes a an object that is able to use the methods defined in the task collect class
    task6 = task_collect.task_collect(ADC_share, ADC_vel_share, euler_angle_share, ang_vel_share, collect_share)
    
    ## Creates a variable that will store the value returned from the task IMU. This value is then
    # used to specify if the IMU is calibrated or not
    breaks = 0
    ## Creates a variable that will store the value returned from the task Panel. This value is then
    # used to specify if the Panel is calibrated or not
    Stop = 0
    
    while(True):
        try:
            Stop = task5.run()
            breaks = task4.run()
            if breaks or Stop == 1:
                break
            else:
                pass
            
            task3.run()
            task2.run()
            task1.intro()
            task6.collect()
                    
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
