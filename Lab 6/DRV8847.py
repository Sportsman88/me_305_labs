''' @file DRV8847.py
    @brief An motor driver class for the DC motors
    @details This files creates two classes that are used to control the motors for the touch panel.
             The first class specifies the methods that are common between all motors that are run. 
             This allows for all motors to have the same on and off functionality. The next class
             specifies the methods that are used to run each motor respectively.
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/8/21
    @copyright license term
''' 
import pyb
class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV88477
        motor driver and to create one or more objects of the
        Motor class which can be used to perform motor9control.
        
        Refer to the DRV8847 datasheet here:
        https://www.ti.com/lit/ds/symlink/drv8847.pdf13
    '''
    
    def __init__ (self):
       ''' @brief      Initializes and returns a DRV8847 object.17
           @details Creates a class that is used to interface with a motor and
                    set its duty cycle. The class has 3 methods. One to construct
                    the motor object, one to enable to motors to run, and one to disable
                    the motors from running.
       '''  
       ## Creates a variable that sets the duty of the motors to 0.
       self.duty = 0
    def enable (self):
       ''' @brief      Brings the DRV8847 out of sleep mode.
           @ details This method when run turns the duty of the motors to 0, resetting the
                     motors to be on but not move.
       '''
       print('Motors Enabled')
       self.duty = 0
      
    def disable (self):
       ''' @brief      Puts the DRV8847 in sleep mode.
           @details This method when ran will disable the motors by setting the motor
                    duty to 0
       '''
       print('Motors Disabled')
       self.duty = 0
   
    def motor (self, A, PinA, B, PinB):
       ''' @brief      Initializes and returns a motor object associated with the DRV8847.
           @details This method when called sets the values for the motor that will be
                    called allowing for the motor class to be run.
           @param A This is the channel to the timer that is connected to the first motor
                  and enables the motor to run
           @param Pin A This is the pin that the channel A is connected to
           @param B This is the channel to the timer that is connected to the first motor
                  and enables the motor to run
           @param Pin B This is the pin that the channel A is connected to
           @return     An object of class Motor39
       '''         
       return Motor(A, PinA, B, PinB)
   
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given 45DC motor.
    '''      
    def __init__ (self, A, PinA, B, PinB):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method53DRV8847.motor().
        ''' 
        ## Defines the frequency that the motor will run at
        self.freq = 25000 
        ## Defines the timer that the motor will use
        self.timX = pyb.Timer(3, freq = self.freq)
        ## Sets a channel of the time that will be used to control the motor
        self.tXchA = self.timX.channel(A, pyb.Timer.PWM, pin=PinA)
        ## Sets a channel of the time that will be used to control the motor
        self.tXchB = self.timX.channel(B, pyb.Timer.PWM, pin=PinB) 
      
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor65
         '''          
        self.duty = duty
        if self.duty > 0:
            self.tXchA.pulse_width_percent(100)
            self.tXchB.pulse_width_percent(100-self.duty)
        elif self.duty < 0:
            self.tXchB.pulse_width_percent(100)
            self.tXchA.pulse_width_percent(100+self.duty)
        elif self.duty == 0:
            self.tXchB.pulse_width_percent(self.duty)
            self.tXchA.pulse_width_percent(self.duty)  