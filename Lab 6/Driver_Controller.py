''' @file           Driver_Controller.py
    @brief A driver for a motor controller
    @details This file creates a controller class that contains 3 methods that
             can be used in other files to converge the motor to desired output 
             speed in radians per second. It contains a contructor method to
             instantiate each class object. A run method to run the P controller.
             A get Kp value mthod to return the current set P gain value. A set Kp 
             value method to set the P gain to an inputed value.
             \image html IMG_8178.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/17/21
'''

from ulab import numpy as np
class Driver_Controller():
     ''' @brief Interface with developed P controller
         @details Creates a class that can be called into other python files that
                 is used to interface and produce a PWM value that converges to oreint the
                 panel and ball to equilibrium.The class contains 3 methods. One to construct a 
                 controller object, one to run the controller and output a PWM value
                 that the motor driver reads, one to return the current P gain value
                 and one to set the P gain of the controller to a specified value,
     '''
     def __init__(self):
        ''' @brief constructs the controller object
            @details Instantiates a controller object that contains 3 different
                     methods that can be used in other python files. 
        '''
        ## Initialisation of Kp gain variable
        self.Kp = 1
        
        ## Initialisation of the allowable error variable in the controller
        self.error = 0
        
        ## Saturation High Limit
        self.sat_max = 25
        
        ## Saturation Low Limit
        self.sat_min = -25
        
        ## setting variable value to 0
        self.T = 0

        ## setting the resistance of the motor
        self.R = 2.21
        
        ## setting the voltage of the motor
        self.Vdc = 12
        
        ## setting the amperage to torque conversion of the motor
        self.Kt = 0.0138 #Nm/A
        
     def run(self, mes):
        ''' @brief Updates the controller
            @details Creates a run method that when ran will update the controller
                     with the desired equilbrium point for the panel and ball.
            @param mes inputs the position of the ball, angle of the panel,
                        velocity of the ball, and angular velocity of the panel.
            @return returns the PWM value that will ball the ball and panel
        '''        
        
        ## Initialisation of measured speed variable
        self.mes = mes
        
        # Controller Math
        self.error = np.array(self.mes)
        self.T = np.dot((self.Kp),self.error.T)
        self.V = ((self.R*100)/(4*self.Kt*self.Vdc))*(-self.T)
        
        
        # Vy Saturation Limit
        if self.V > self.sat_max:
            self.V = self.sat_max
        if self.V < self.sat_min:
            self.V = self.sat_min
           
        return self.V

     def get_Kp(self):
        ''' @brief Returns the P gain value.
            @details Creates a method when called retursn the currently set
                     P gain value of the controller
            @return returns the PWM value that will converge the motor speed to the desired speed
        '''  
        return self.Kp

     def set_Kp(self, Kp):
        ''' @brief Sets the P gain value.   
            @details Creates a method that when called set the P gain value
                     to the specified input value.
            @param Kp The new P gain value of the controller
        '''
        self.Kp = Kp
  
