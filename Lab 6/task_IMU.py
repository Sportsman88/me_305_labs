""" @file  IMU.py
    @brief A task used to retrieve the euler angles and angular velocity from an IMU
    @details This class contains 3 methods that are used to calibrate, gather and send 
             out the data read from an IMU. This first method instantiates the variables
             that are used in this class. The next method runs the task and collects data
             from the IMU. It first checks to see if there is a file containing the IMU 
             coeficients and if there then it writes these values to the IMU if there 
             isn't it checks to see if the IMU is calibrated and when it is will write a 
             file with those values. Finally the method will share the Euler Angles and 
             angular velocities. The final method enables transition between the two 
             states in the previous method
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/11/21
    @copyright license term
"""
import utime, IMU, os
from pyb import I2C
from micropython import const

## State 0 of the user interface task
S0_INIT         = const(0)

## State 1 of the user interface task
S1_SEND_DATA    = const(1)

class task_IMU():
    '''@brief Creates a class that gathers and shares the data from an IMU
       @details This class when created defines all of the variables to be used. The 
                class then can be used to check if the IMU is calibrated and if it is 
                then moves into sharing the values of the IMU.
    '''
    def __init__(self, euler_angle_share, ang_vel_share):
        '''@brief Constructs an IMU task
            @details Creates several variables that are used to calibrate and retrieve the 
                     euler angle reading and angular velocity for the pitch and roll angles
                     from the IMU.
            @param euler_angle_share inputs a share that retrieves the euler angles from the IMU
            @param ang_vel_share inputs a share that retrieves the angular velocity from the IMU
        '''
        ## Initalizes the use of an IMU to gather positional data
        self.i2c = IMU.IMU(I2C(1, I2C.MASTER))
        self.i2c.Op_mode(2) # sets the IMU to NDOF mode
        
        ## sets the data collection period
        self.period = 5
        
        ## sets when the next time data will be collected
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## Creates a share to be able to sends the angular velocity data to the task controller
        # and the task collect
        self.ang_vel_share = ang_vel_share
        
        ## Creates a share to be able to sends the euler angle data to the task controller
        # and the task collect
        self.euler_angle_share = euler_angle_share
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_INIT
        
        ## sets when the next time data will be collected
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## Creates a byte array of 22 bytes that can contain the calibration constants from the IMU
        self.cal_const = bytearray(22)
    
    def run(self):
        '''@brief Checks for calibration and then shares out the euler angles and angular velocities
           @details This method when run first checks to see if there a file containing the calibration
                    constants and if there is then it writes these values to the IMU. If there is not
                    a file then it checks to see if the IMU is calibrated and if it is then it writes
                    a file and puts in the calibration coefficents. From there it transitions into a 
                    new state where it sends out the euler angles and angular velocities for the pitch
                    and roll values.
        '''
        ## Creates a current time that updates everytime the code is ran. This current
        # time sets the rate at which this program will run
        current_time = utime.ticks_ms()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                
                ##Creates a variable with a common file name to check for the calibration
                # constants that were printed from a previous run with the IMU
                filename = "IMU_cal_coeffs.txt"
                if filename in os.listdir():
                    with open(filename, 'r') as f:
                        print('Reading Calibration Constants from IMU')
                        ## Creates a variable that reads the first line of the file
                        cal_data_string = f.readline()
                        ##Creates a variable containing the calibration values from the file and
                        # turns them into integer values
                        self.cal_values = [int(cal_value) for cal_value in cal_data_string.strip().split(',')]
                        ## Turns the calibration values into a bytearray
                        self.cal_const = bytearray(self.cal_values)
                        self.i2c.write_cal_const(self.cal_const)
                        self.transition_to(S1_SEND_DATA)
                        print("Welcome to the Nucleo Hardware!", 
                              "Type 1 to begin balacning the board",
                              "Type g or G to start collecting data:",
                              "Type s or S to end collection prematurely:", sep="\n")

                else:
                    if self.i2c.calibration_status() == b'\xff':
                        with open(filename, 'w') as f:
                            # Perform manual calibration
                            print("The Calibration Constants are:")
                            print(self.i2c.get_cal_const())
                            print('Writing calibration constoants to IMU_cal_coeffs')
                            # Then, write the calibration coefficients to the file
                            # as a string. The example uses an f-string, but you can
                            # use string.format() if you prefer
                            #print((Kxx, Kxy, Kyx, Kyy, Xc, Yc))
                            f.write(self.i2c.get_cal_const())
                            print('Calibration Complete')
                            self.state = S1_SEND_DATA
                            print("Welcome to the Nucleo Hardware!", 
                                  "Type 1 to begin balacning the board",
                                  "Type g or G to start collecting data:",
                                  "Type s or S to end collection prematurely:", sep="\n")
                    else: 
                        print('Please Calibrate the IMU')
                        self.i2c.calibration_status()
        
            elif self.state == S1_SEND_DATA:
                self.euler_angle_share[0].write(self.i2c.read_Euler_angles()[0])
                self.euler_angle_share[1].write(self.i2c.read_Euler_angles()[1])
                self.ang_vel_share[0].write(self.i2c.read_ang_vel()[0])
                self.ang_vel_share[1].write(self.i2c.read_ang_vel()[1])
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param new_state The state to transition to.
        '''
        self.state = new_state