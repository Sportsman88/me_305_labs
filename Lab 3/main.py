''' @file           main.py
    @brief Runs the main program and creates all objects to run the encoder and motor
    @details This program starts up on a reboot and runs through. It first creates
             all of the class objects from the task user, task encoder, task motor and shares 
             classes and then uses them in a loop fashion to retrieve data and share data
             between them both. Finally to exit the user must enter a keyboard interupt.
             Below is a diagram showing how data is retrieved and shared
             \image html IMG_8179.jpg "Task and Shares Diagram"
             Below is data showing 2 runs at different duty cycles and the data collected 
             using the 'g' function.
             \image html Lab3_1.JPG "Postion Speed of Motor at 50% Duty Cycle"
             \image html Lab3_2.JPG "Postion Speed of Motor at 100% Duty Cycle"
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/03/21
'''

import task_user
import task_encoder
import task_motor
import shares

#Running the code
if __name__ =='__main__':
    
    ## Initalizes a variable that is able to share Encoder 1 Data between tasks
    encoder1_share = (shares.Share(0), shares.Share(0))
    
    ## Initalizes a variable that is able to share Encoder 2 Data between tasks
    encoder2_share = (shares.Share(0), shares.Share(0))
    
    ## Initalizes a variable that is able to share Encoder 1 Zero Position Flags between tasks
    zflag1 = shares.Share(False)
    
    ## Initalizes a variable that is able to share Encoder 2 Zero Position Flags between tasks
    zflag2 = shares.Share(False)
    
    ## Initalizes a variable that is able to share Motor 1 Duty Cycle Data between tasks
    motor1_share = shares.Share(0)
    
    ## Initalizes a variable that is able to share Motor 2 Duty Cycle Data between tasks
    motor2_share = shares.Share(0)
    
    ## Initalizes a variable that is able to share Motor Fault Flags between tasks
    fault_share = shares.Share(False)
    
    ## Initalizes an object that is able to use the methods defined in the task
    #  user class
    task1 = task_user.task_user(motor1_share, encoder1_share, motor2_share, encoder2_share, zflag1, zflag2, fault_share)
    
    ## Initalizes an object that is able to use the methods defined in the task
    #  encoder class
    task0 = task_encoder.task_encoder(encoder1_share, encoder2_share, zflag1, zflag2)

    ## Initalizes an object that is able to use the methods defined in the task
    #  motor class    
    task2 = task_motor.task_motor(motor1_share, motor2_share, fault_share)
    
 
    
    while(True):
        try:

            task0.run()
            task1.intro()
            task2.run()
            
                    
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
