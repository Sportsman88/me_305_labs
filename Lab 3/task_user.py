''' @file           task_user.py
    @brief A task used to check for and implement specific keyboard commands
    @details This task when called will create a task user object from the task
             user class. This class contains 7 methods to perform different tasks.
             The first method initalizes the task user object. The next method
             when called prompts the user with the specific commands they can use
             and then transition into waiting for a command before running any code.
             From there dependsing on the key stroke it sends data back to the main
             or sends the user to the third, fourth, fifth or sixth method. Depending 
             on the motor choice and decision to set the motor duty cycle or collect data
             will determine which method the program will run next. The third or fourth 
             method the program runs through collecting data for 30 seconds or until a 
             stop command is issued. The third method refers to motor 1 and fourth to 
             motor 2. From there it moves on to the seventh method that prints the data 
             in a list and then runs back to the second method. The fifth and sixth method
             waits for the user to enter valid values to set the motor duty cycle. Once the 
             duty cycle has been set the program runs back the second method.
             \image html IMG_8178.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/03/21
'''
import pyb, utime, array
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)

## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)

## State 2 of the user interface task
S2_COLLECT_DATA     = const(2)

## State 3 of the user interface task
S3_COLLECT_DATA     = const(3)

## State 4 of the user interface task
S4_COLLECT_INPUT     = const(4)

## State 5 of the user interface task
S5_COLLECT_INPUT     = const(5)

## State 6 of the user interface task
S_print = const(6)


class task_user():
     ''' @brief     A task for keyboard inputs to call for encoder data
         @details   This class is a task taking in a user input through 
                    a single keyboard key input and displaying the related 
                    information to the input 
     '''
     def __init__(self, motor1_share, encoder1_share, motor2_share, encoder2_share, zflag1, zflag2, fault_share):
         ''' @brief         Initializes task_user
             @details       This initiates an User objects and prepare 
                            keyboard serial input and other variables
             @param         motor1_share inputs a share that links the data from this file
                            to the motor task so they can communicate
             @param         encoders1_share inputs a share that links the data from this file
                            to the encoder task so they can communicate
             @param         motor2_share inputs a share that links the data from this file
                            to the motor task so they can communicate
             @param         encoders2_share inputs a share that links the data from this file
                            to the encoder task so they can communicate
             @param         fault_share inputs a share that links the data from this file
                            to the motot task so they can communicate and clear any faults that
                            occur
         '''
         ## A serial port to use for user I/O
         self.ser = pyb.USB_VCP()
        
         ## The state to run on the next iteration of the finite state machine
         self.state = S0_INIT
         
         ## creates a list that will contain time data for 30 seconds
         self.time_array = array.array('i', range(301))
         
         ## creates a list that will contain position data in radians for 30 seconds
         self.pos_array = array.array('d', range(301))
         
         ## creates a list that will contain speed data in rad/sec for 30 seconds
         self.speed_array = array.array('d', range(301))
         
         ## creates a list that will contain time difference data for 30 seconds
         self.diff_array = array.array('d', range(301))
         
         ## sets the duty input from the serial to zero
         self.duty = '0'
         
         ## sets the data collection period or frequency
         self.period = 100
         
         ## sets when the next time data will be collected
         self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
         
         ## sets encoder 1 data share varaible
         self.encoder1_share = encoder1_share
         
         ## sets motor 1 data share varaible
         self.motor1_share = motor1_share
         
         ## sets encoder 2 data share varaible
         self.encoder2_share = encoder2_share
         
         ## sets motor 2 data share varaible
         self.motor2_share = motor2_share
         
         ## sets encoder 1 zero flag share varaible
         self.zflag1 = zflag1
         
         ## sets encoder 2 zero flag share varaible
         self.zflag2 = zflag2
         
         ## sets motors fault flag share varaible
         self.fault_share = fault_share
         
         ## sets the number of prints the tasks runs to zero
         self.print_count = 0
         
         
     def intro(self):
         ''' @brief     Introduce and run the user task.
             @details   Uses a finite state machine to interact with the user
                        inputs.Characters are entered through a serial
                        port that cmmunicating through a USB keyboard.
         '''
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
             if self.state == S0_INIT:
                 print("Welcome to the Nucleo Hardware!", 
                       "Type z to zero the position of encoder 1.",
                       "Type Z to zero the position of encoder 2.",
                       "Type p for the position of encoder 1.",
                       "Type P for the position of encoder 2.",
                       "Type d for the delta for encoder 1.",
                       "Type D for the delta for encoder 2.",
                       "Type m to set the duty cycle for motor 1.",
                       "Type M to set the duty cycle for motor 2.",
                       "Type c or C to clear any faults found.",
                       "Type g to collect encoder 1 data for 30 sec.",
                       "Type G to collect encoder 2 data for 30 sec.",
                       "Type s or S to end collection prematurely:", sep="\n")
                 
                 self.transition_to(S1_WAIT_FOR_CHAR)
             elif self.state == S1_WAIT_FOR_CHAR:
                    if self.ser.any():
                   
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                        char_in = self.ser.read(1).decode()
                    
                        if (char_in == 'z'):
                            print('Encoder 1 Position Zeroed')
                            self.zflag1.write(True)
                        
                        elif (char_in == 'Z'):
                            print('Encoder 2 Position Zeroed')
                            self.zflag2.write(True)
                        
                        elif (char_in == 'p'):
                            print(self.encoder1_share[0].read())
                        
                        elif (char_in == 'P'):
                            print(self.encoder2_share[0].read())
                        
                        elif (char_in == 'd'):
                            print(self.encoder1_share[1].read())
                        
                        elif (char_in == 'D'):
                            print(self.encoder2_share[1].read())
                        
                        elif (char_in == 'm'):
                            print('Enter Motor Duty for Motor 1')
                            self.duty = '0'
                            self.transition_to(S4_COLLECT_INPUT)
                        
                        elif (char_in == 'M'):
                            print('Enter Motor Duty for Motor 2')
                            self.duty = '0'
                            self.transition_to(S5_COLLECT_INPUT)
                        
                        elif (char_in == 'c' or char_in == 'C'):
                            print('Clear Fault')
                            self.fault_share.write(True)
                        
                        elif (char_in == 'g'):
                            print ('Collecting Data from Encoder 1')
                            ## counts the number of times data is collected
                            self.runs = 0
                            self.transition_to(S2_COLLECT_DATA)
                            
                            
                            
                        elif (char_in == 'G'):
                            print ('Collecting Data from Encoder 2')
                            ## counts the number of times data is collected
                            self.runs = 0
                            self.transition_to(S3_COLLECT_DATA)
                            
                
             elif self.state == S2_COLLECT_DATA:                     
                     if self.runs < (30e3/self.period)+1:
                         
                         ## records position data from encoder 1
                         self.pos_array[self.runs] = self.encoder1_share[0].read()
                         
                         ## records speed data from encoder 1
                         self.speed_array[self.runs] = self.encoder1_share[1].read()
                         
                         ## records time data during collection
                         self.time_array[self.runs] = utime.ticks_ms()
                         
                         self.runs += 1
                         
                     if self.ser.any():
                        ## Creates a string variable that contains any input data from
                        #  strokes on the keyboard as the letter that is pressed.
                        char_in = self.ser.read(1).decode()
                        if (char_in == 's' or char_in == 'S'):
                            print('Data Collection Ended Early')
                            self.transition_to(S_print)
                     elif self.runs >= (30e3/self.period)+1:
                        self.transition_to(S_print)
                     else: 
                        pass

             elif self.state == S3_COLLECT_DATA:
                 
                     if self.runs < (30e3/self.period)+1:
                         
                         ## records position data from encoder 2
                         self.pos_array[self.runs] = self.encoder2_share[0].read()
                         
                         ## records speed data from encoder 2
                         self.speed_array[self.runs] = self.encoder2_share[1].read()
                         
                         self.time_array[self.runs] = utime.ticks_ms()
                         self.runs += 1
                         
                     if self.ser.any():
                         ## Creates a string variable that contains any input data from
                         #  strokes on the keyboard as the letter that is pressed.
                         char_in = self.ser.read(1).decode()
                         if (char_in == 's' or char_in == 'S'):
                            print('Data Collection Ended Early')
                            self.transition_to(S_print)
                     elif self.runs >= (30e3/self.period)+1:
                        self.transition_to(S_print)
                     else: 
                         pass
                    
             elif self.state == S_print:
                            if self.print_count < self.runs:                              
                                self.diff_array[self.print_count] = utime.ticks_diff(self.time_array[self.print_count], self.time_array[0])/1000
                                print(self.diff_array[self.print_count], self.pos_array[self.print_count], self.speed_array[self.print_count])
                                self.print_count += 1  
                            else:
                                ## empties arrays and counts and returns to state 1
                                self.time_array = array.array('i', range(301))
                                self.pos_array = array.array('d', range(301))
                                self.speed_array = array.array('d', range(301))
                                self.diff_array = array.array('d', range(301))
                                self.print_count = 0
                                self.transition_to(S1_WAIT_FOR_CHAR)
                
                            
             elif self.state == S4_COLLECT_INPUT:
                if self.ser.any():
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '\x7F':
                        if len(self.duty) == 1:
                            self.duty = '0'
                            pass
                        else:
                            self.duty = self.duty[:-1]
                    elif char_in == '\r' or '\n':
                        self.motor1_share.write(float(self.duty))
                        print("")
                        self.transition_to(S1_WAIT_FOR_CHAR)
                
             elif self.state == S5_COLLECT_INPUT:
                if self.ser.any():
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit() == True:
                        self.duty += char_in
                        self.ser.write(char_in)
                    elif char_in == '-':
                        if len(self.duty) == 1:
                            self.duty = '-'
                            self.ser.write(char_in)
                        else:
                            pass
                    elif char_in == '\x7F':
                        if len(self.duty) == 1:
                            self.duty = '0'
                            pass
                        else:
                            self.duty = self.duty[:-1]
                    elif char_in == '\r' or '\n':
                        self.motor2_share.write(float(self.duty))
                        print("")
                        self.transition_to(S1_WAIT_FOR_CHAR)                 
             self.next_time = utime.ticks_add(self.next_time, self.period)
                
                
     def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        self.state = new_state