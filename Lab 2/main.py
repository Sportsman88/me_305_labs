''' @file           main.py
    @brief Runs the main program and creates all objects to run the encoder
    @details This program starts up on a reboot and runs through. It first creates
             all of the class objects from the task user and task encoder classes
             and then uses them in a loop fashion to retrieve data and share data
             between them both. Finally to exit the user must enter a keyboard
             interupt.
             \image html IMG_8179.jpg "Task and Shares Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
    @copyright license term
'''

import task_user
import task_encoder

#Running the code
if __name__ == '__main__':

    ## Initalizes a an object that is able to use the methods defined in the task
    #  user class
    task1 = task_user.task_user()
    
    ## Initalizes a an object that is able to use the methods defined in the task
    #  encoder class
    task0 = task_encoder.task_encoder()
    
    ## Creates a variable to signal when the position of the encoder should be 0
    z_flag = False
    
    
    while(True):
        try:
            ## Creates a variable that takes in the z flag boolean value and returns 
            #  the position and the delta of the encoder to use in task user.
            inputs = task0.run(z_flag)
            z_flag = task1.intro(inputs)
            
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')

