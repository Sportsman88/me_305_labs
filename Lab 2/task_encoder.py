""" @file task_encoder.py
    @brief A task used to retrieve data from an encoder and set the encoder reading to 0
    @details This file creates a task that is used to operate an encoder and provide
             its data in a usable shared format. The class has 2 methods in it. One
             method is used to instatntiate the task encoder objects and the other
             method is used to run the encoderr and retrieve the data from the encoder
             as well as check if the position of the encoder should be zeroed.
             \image html IMG_8180.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
"""
import encoder, pyb

class task_encoder():
    ''' @brief Encoder task for Lab 0x02
        @details Implements a finite state machine that contnuosuly updates 
                 the read position of the encoder and returns all data to the 
                 main function
    '''
    def __init__(self):
        ''' @brief Constructs an Encoder Task
            @details The encoder task implements the finite state machine and
                     links all of the tasks to the specifed encoder
        '''
        
        ## Initalizes the use of an encoder to generate data
        self.encoders = encoder.encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
        
        
    def run(self,z_flag):
        ''' @brief Runs one iteration of the FSM every time it is called
            @details This method runs the encoder FSM machine 1 time and returns
                     the position that the encoder reads of the shaft
            @param z_flag A boolean flag used to tell the encoder to set its 
                   read position to zero
            @return returns the position of the shaft that the encoder is placed on
        '''
        self.encoders.update()
        if z_flag == True:
            self.encoders.set_position(0)
       
        return self.encoders.get_position(), self.encoders.get_delta()