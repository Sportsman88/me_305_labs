''' @file           main.py
    @brief Runs the main program and creates all objects to run the IMU
    @details This program starts up on a reboot and runs through. It first creates
             all of the class objects from the IMU class and then uses them in a 
             loop fashion to retrieve data and share data between them both. 
             Finally to exit the user must enter a keyboard interupt.
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/11/21
    @copyright license term
'''    
import IMU, utime, shares
from pyb import I2C
   
if __name__ == '__main__':
    ## Initalizes a shares object that contains the calibration status data to share
    cal_stat_share = shares.Share(0)
    
    ## Initalizes the use of an IMU to gather positional data
    i2c = IMU.IMU(I2C(1, I2C.MASTER), cal_stat_share)
    
    i2c.Op_mode(2)
    i2c.get_cal_const()

    while(True):
        try:
            if cal_stat_share.read() == b'\xff':
                i2c.read_Euler_angles()
            else:
                i2c.calibration_status()
            utime.sleep_ms(100)
        except KeyboardInterrupt:
            break
    print('Program Terminating')