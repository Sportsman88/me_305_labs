""" @file  IMU.py
    @brief An IMU driver class for the BNO055 IMU
    @details Objects from this class are used to set up and retrieve data from
             BNO055 IMU. The methods provided below allow for the user to create
             an IMU object, change the operation mode of the device, retrieve the
             calibration status of the device, print out the calibration constants,
             write over the calibration constants, retrieve the Euler Angles, and
             retrieve the angular velocity of the device
    @author Cade Liberty
    @author Chris Suzuki
    @date 11/11/21
    @copyright license term
"""
from pyb import I2C
import struct

class IMU:
    def __init__(self, i2c, cal_stat_share):
        '''@brief Constructs an IMU task
           @details Creates a series of variables that are used to read and retrieve
                    values from the BNO055 IMU device. 
           @param i2c inputs the preconfigured I2C device to run the IMU
           @param cal_stat_share inputs a share that links the data from this file
                   to the main so they can communicate
        '''
        ##Creates an I2C type object to run
        self.i2c = i2c 
        self.i2c.init(I2C.MASTER)     # Turn on the sensor
        
        ## Creates a Bytearray for the Euler Angles to be wrriten into
        self.Eul_buff = bytearray(6)  # set a byte array of length 6
        
        ## Creates a Bytearray for the Angular Velocity to be wrriten into
        self.ang_buff = bytearray(6)  # set a byte array of length 6
        
        ## Creates a Bytearray for the calibration constants to be wrriten into
        self.cal_const = bytearray(22) # set a byte array of length 22
        
        ##Creates a calibration status share that shares the calibration status to the main file
        self.cal_stat_share = cal_stat_share
    
    def Op_mode(self, Op_mode):
        '''@brief Sets and changes the operation mode of the IMU
           @details This method when called first turns on the IMU and sets it to confgure mode.
                    From there the IMU is set to whatever input is placed into the input value,
                    typically the NDOF mode.
           @param Op_mode Takes in a value to set the operation mode of the IMU
        '''
        self.i2c.mem_write(0b0000, 0x28, 0x3D) # sets configure mode
        if Op_mode == 1:
            print('Setting Operation mode to Configure mode')
            self.i2c.mem_write(0b0000, 0x28, 0x3D) #set the IMU to configure mode
        elif Op_mode == 2:
            print('Setting Operation mode to NDOF mode')
            self.i2c.mem_write(0b1100, 0x28, 0x3D) #set the IMU to IMU mode
        else:
            pass
    
    def calibration_status(self):
        '''@brief Retrieves and prints the calibration status of the IMU
           @details This method reads the calibration status of the IMU and then prints
                    out the values as a series of 4 values. For this device a fully
                    calibrated system will read (3, 3, 3, 3) and each of the values will
                    range from 0 to 3.
        '''
        print('Calibration status is:')
        ## creates a bytes data value that will be read and printed out
        cal_bytes = self.i2c.mem_read(1, 0x28, 0x35) #read the calibration data
        ## Combines the bytes data from the IMU to readable binary data
        cal_status = ( cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)
        self.cal_stat_share.write(cal_bytes)
        print(cal_status) #print the euler angles as decimal numbers
        
        
    def get_cal_const(self):
        '''@brief Constructs an IMU task
           @details Creates a series of variables that are used to read and retrieve
                    values from the BNO055 IMU device. 
           @param i2c inputs the preconfigured I2C device to run the IMU
           @param cal_stat_share inputs a share that links the data from this file
                   to the main so they can communicate
        '''
        print('Calibration constants are:')
        print(self.i2c.mem_read(self.cal_const, 0x28, 0x55))
    
    def write_cal_const(self, data):
        ''' @brief Writes in the calibration data that is input to the system
            @details When called this method sets the operation mode into configure mode.
                     From there the system writes in the given calibration constants to the 
                     IMU.
            @param data is the calibration data in binary and placed into a bytearray of 22 values that is 
                   needed to be written into the calibration coefficents
        '''
        self.i2c.mem_write(0b0000, 0x28, 0x3D) # set the mode to configure mode
        self.i2c.mem_write(data, 0x28, 0x55) # Write to the LSB of the x accelerometer offset
        print('Calibration Constants set. Retunring to NDOF mode.')
        self.i2c.mem_write(0b1100, 0x28, 0x3D)
    
    def read_Euler_angles(self):
        '''@brief Reads the Euler Angles from the IMU and prints them
           @details When called this method will read the Euler Angles from the IMU. From
                    there the code will use the unpack method from the struct class and
                    combine the data into readable 16 bit values which are then printed out.
        '''
        print('Reading Euler Angle Value')
        self.i2c.mem_read(self.Eul_buff, 0x28, 0x1A) #Read the Euler Angles from the sensor
        ## Creates a series of 3 16-bit numbers constained the Euler Angle info from the IMU
        Euler_Angles = struct.unpack('hhh', self.Eul_buff) #upnack and combine the the Euler angles into a tuple of integers
        print(Euler_Angles[0]/16, Euler_Angles[1]/16, Euler_Angles[2]/16) #print the euler angles as decimal numbers
    
    def read_ang_vel(self):
        '''@brief Reads the Angular Velocity from the IMU and prints them
           @details When called this method will read the Angular Velocity from the IMU. From
                    there the code will use the unpack method from the struct class and
                    combine the data into readable 16 bit values which are then printed out.
        '''
        print('Reading Angular Velocity Values')
        self.i2c.mem_read(self.ang_buff, 0x28, 0x14) #Read the Acelerometer data from the sensor
        ## Creates a series of 3 16-bit numbers constained the Angular Velocity info from the IMU
        Angular_Velocity = struct.unpack('hhh', self.ang_buff) #upnack and combine the the Angular Velocity into a tuple of integers
        print(Angular_Velocity[0]/16, Angular_Velocity[1]/16, Angular_Velocity[2]/16) #print the Angular Velocity as decimal numbers
    